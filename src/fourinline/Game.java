/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fourinline;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Alow, M
 */
class Game {
    public final static int HUMAN = Board.PLAYER2;
    public final static int COMPUTER = Board.PLAYER1;
    private final Board board;
    private Space spaces[][];
    private final ArrayList<Combination> combinationList = new ArrayList<>();
    private int score = 0;
    private int colOrder[];

    public Game(Board board) {
        this.board = board;
        initSpaces();
        generateHorizontalCombinations();
        generateVerticalCombinations();
        generateDiagonalCombinations();
        initColOrder();
        
    }

    private void initColOrder() {
        int colNo = board.getColNo();
        colOrder = new int[colNo];
        int order = 0;
        int right = board.getColNo() / 2;
        int left = right;
        colOrder[order] = right;
        ++order;
        if (colNo % 2 == 0) {
            --left;
            colOrder[order] = left;
            ++order;
        }
        for (int i = 1; i < (colNo + 1) / 2; ++i) {
            colOrder[order] = right + i;
            ++order;
            colOrder[order] = left - i;
            ++order;
        }
        System.out.println(Arrays.toString(colOrder));
    }

    private void initSpaces() {
        spaces = new Space[board.getRowNo()][board.getColNo()];
        for (int row = 0; row < board.getRowNo(); ++row) {
            for (int col = 0; col < board.getColNo(); ++col) {
                spaces[row][col] = new Space();
                spaces[row][col].setRow(row);
                spaces[row][col].setCol(col);
            }
        }
    }

    boolean isTerminal() {
        for (Combination combination : combinationList) {
            if (combination.winner() != Board.FREE)
                return true;
        }
        return board.isFilled();
    }

    void play(Move move) {
        board.play(move);
        int col = move.getCol();
        int row = move.getRow();
        Space space = spaces[row][col];
        int oldScore = space.score();
        space.notifyObservers(move);
        int newScore = space.score();
        score += newScore - oldScore;
    }

    int evaluation() {
        return score;
    }

    Iterable<Move> moves(int player) {
        ArrayList<Move> res = new ArrayList<>();
        for (int col : colOrder) {
            Move move = board.availableMove(col, player);
            if (move == null) continue;
            res.add(move);
        }
        return res;
    }

    void undo(Move move) {
        Move reversedMove = move.reversedMove();
        play(reversedMove);
    }

    private void generateHorizontalCombinations() {
        for (int row = 0; row < board.getRowNo(); ++row) {
            for (int col = 0; col < board.getColNo() - 3; ++col) {
                Combination combination = new Combination();
                combinationList.add(combination);
                for (int i = 0; i < 4; ++i) {
                    Space space = spaces[row][col + i];
                    space.registerObserver(combination);
                    combination.addSpace(space);
                }
            }
        }
    }

    private void generateVerticalCombinations() {
        for (int row = 0; row < board.getRowNo() - 3; ++row) {
            for (int col = 0; col < board.getColNo(); ++col) {
                Combination combination = new Combination();
                combinationList.add(combination);
                for (int i = 0; i < 4; ++i) {
                    Space space = spaces[row + i][col];
                    space.registerObserver(combination);
                    combination.addSpace(space);
                }
            }
        }
    }

    private void generateDiagonalCombinations() {
        for (int row = 0; row < board.getRowNo() - 3; ++row) {
            for (int col = 0; col < board.getColNo() - 3; ++col) {
                Combination combination1 = new Combination();
                combinationList.add(combination1);
                for (int i = 0; i < 4; ++i) {
                    Space space = spaces[row + i][col + i];
                    space.registerObserver(combination1);
                    combination1.addSpace(space);
                }
                Combination combination2 = new Combination();
                combinationList.add(combination2);
                for (int i = 0; i < 4; ++i) {
                    Space space = spaces[row + i][col + 3 - i];
                    space.registerObserver(combination2);
                    combination2.addSpace(space);
                }
            }
        }
    }
    
}
