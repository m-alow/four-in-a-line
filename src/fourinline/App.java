/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fourinline;

import java.util.Scanner;

/**
 *
 * @author Alow, M
 */
public class App {
    private Board board;
    private Game game;
    private Computer computer;
    
    public App(int rowNo, int colNo, int maxDepth) {
        board = new Board(rowNo, colNo);
        game = new Game(board);
        computer = new Computer(maxDepth);
    }
    
    public void play() {
        print();
        System.out.println("");
        
        int player = Game.COMPUTER;
        while (!game.isTerminal()) {
            if (player == Game.COMPUTER) {
                System.out.println("Computer is thinkng...");
                long time = -System.currentTimeMillis();
                Move move = computer.chosenMove(game);
                time += System.currentTimeMillis();
                game.play(move);
                print();
                printStatistics((int)time);
                System.out.println("\n");
            } else {
                Move move = humanMove();
                game.play(move);
                print();
                System.out.println("\n");
            }
            player = nextPlayer(player);
        }
        if (game.evaluation() == 0) {
            System.out.println("Draw");
        } else if (game.evaluation() > 0) {
            System.out.println("Computer wins");
        } else {
            System.out.println("Bitch wins");
        }
    }
    
    private int nextPlayer(int player) {
        if (player == Game.COMPUTER) return Game.HUMAN;
        return Game.COMPUTER;
    }
    
    private Move humanMove() {
        Scanner scanner = new Scanner(System.in);
        int col = 0;
        Move move = null;
        while (col <= 0 || col > board.getColNo() || (move = board.availableMove(col - 1, Game.HUMAN)) == null) {
            System.out.print("Choose column: ");
            col = scanner.nextInt();
        }
        return move;
    }
    
    private void print() {
        int[][] b = board.getBoard();
        for (int i = board.getRowNo() - 1; i >= 0; --i) {
            int[] row = b[i];
            System.out.print("| ");
            for (int j = 0; j < row.length; ++j) {
                int p = row[j];
                char output = '.';
                if (p == Game.COMPUTER) output = '*';
                if (p == Game.HUMAN) output = 'o';
                System.out.print(output + " | ");
            }
            System.out.println("");
        }
        for (int j = 1; j <= board.getColNo(); ++j)
            System.out.print("  " + j + " ");
        System.out.println("");
        System.out.println("Evaluation: " + game.evaluation());
    }
    private void printStatistics(int time) {
        System.out.println("Elapsed time(ms): " + time);
        if (time == 0) time = 1;
        System.out.println("Visited nodes / millisecond: " + computer.getVisitedNodes() / time);
        System.out.println("Visited nodes: " + computer.getVisitedNodes());
        System.out.println("Expanded nodes: " + computer.getExpandedNodes());
        System.out.println("Terminal nodes (max depth): " + computer.getTerminalDepthNodes());
        System.out.println("Terminal nodes (terminal game): " + computer.getTerminalGameNodes());
    }
}
