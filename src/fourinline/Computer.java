/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fourinline;

/**
 *
 * @author Alow, M
 */
public class Computer {
    private int maxDepth;
    private int visitedNodes = 0;
    private int expandedNodes = 0;
    private int terminalDepthNodes = 0;
    private int terminalGameNodes = 0;

    public Computer(int maxDepth) {
        this.maxDepth = maxDepth;
    }
    
    public Move chosenMove(Game game) {
        visitedNodes = 0;
        expandedNodes = 0;
        terminalDepthNodes = 0;
        terminalGameNodes = 0;
        Move bestMove = null;
        int bestMoveValue = Integer.MIN_VALUE;
        for (Move move : game.moves(Game.COMPUTER)) {
            game.play(move);
            int moveValue = min(game, Integer.MIN_VALUE, Integer.MAX_VALUE, 1);
            if (moveValue > bestMoveValue) {
                bestMoveValue = moveValue;
                bestMove = move;
            }
            game.undo(move);
        }
        return bestMove;
    }
    
    public int max(Game game, int alpha, int beta, int depth) {
        ++visitedNodes;
        if (depth == maxDepth) {
            ++terminalDepthNodes;
            return game.evaluation();
        } else if (game.isTerminal()) {
            ++terminalGameNodes;
            return game.evaluation();
        }
        ++expandedNodes;
        
        for (Move move : game.moves(Game.COMPUTER)) {
            game.play(move);
            alpha = Math.max(alpha, min(game, alpha, beta, depth + 1));
            game.undo(move);
            if (alpha >= beta) return beta;
        }
        return alpha;
    }
    
    public int min(Game game, int alpha, int beta, int depth) {
        ++visitedNodes;
        if (depth == maxDepth) {
            ++terminalDepthNodes;
            return game.evaluation();
        } else if (game.isTerminal()) {
            ++terminalGameNodes;
            return game.evaluation();
        }
        
        ++expandedNodes;
        for (Move move : game.moves(Game.HUMAN)) {
            game.play(move);
            beta = Math.min(beta, max(game, alpha, beta, depth + 1));
            game.undo(move);
            if (beta <= alpha) return alpha;
        }
        return beta;
    }

    public int getMaxDepth() {
        return maxDepth;
    }

    public void setMaxDepth(int maxDepth) {
        this.maxDepth = maxDepth;
    }

    public int getVisitedNodes() {
        return visitedNodes;
    }

    public int getExpandedNodes() {
        return expandedNodes;
    }

    public int getTerminalDepthNodes() {
        return terminalDepthNodes;
    }

    public int getTerminalGameNodes() {
        return terminalGameNodes;
    }
    
    
    
}
