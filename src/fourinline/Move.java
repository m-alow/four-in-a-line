/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fourinline;

/**
 *
 * @author Alow, M
 */
public class Move {
    private final int col;
    private final int row;
    private final int player;
    private final int prevPlayer;

    public Move(int row, int col, int player, int prevPlayer) {
        this.row = row;
        this.col = col;
        this.player = player;
        this.prevPlayer = prevPlayer;
    }

    Move(Move move) {
        this(move.row, move.col, move.player, move.prevPlayer);
    }

    public int getCol() {
        return col;
    }
    
    public int getRow() {
        return row;
    }

    public int getPlayer() {
        return player;
    }

    public int getPrevPlayer() {
        return prevPlayer;
    }
    
    public boolean isUndoMove() {
        return prevPlayer != Board.FREE;
    }
    
    public boolean isDoMove() {
        return prevPlayer == Board.FREE;
    }
    
    public Move reversedMove() {
        return new Move(row, col, prevPlayer, player);
    }
}
