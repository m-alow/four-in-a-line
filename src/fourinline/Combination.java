package fourinline;

import java.util.ArrayList;

/**
 *
 * @author Alow, M
 */
public class Combination extends SpceObserver {
    private int human = 0;
    private int computer = 0;
    private final ArrayList<Space> spaceList = new ArrayList<>();

    public ArrayList<Space> getSpaceList() {
        return spaceList;
    }

    public void addSpace(Space space) {
        this.spaceList.add(space);
    }
    
    @Override
    public int score() {
        if (human == 0 && computer == 0) return 0;
        if (human != 0 && computer != 0) return 0;
        
        if (human == 1) return -1;
        else if (human == 2) return -10;
        else if (human == 3) return -100;
        else if (human == 4) return -1000;
        
        if (computer == 1) return 1;
        else if (computer == 2) return 10;
        else if (computer == 3) return 100;
        else return 1000;
    }
    
    public int winner() {
        if (human == 4) return Game.HUMAN;
        if (computer == 4) return Game.COMPUTER;
        return Board.FREE;
    }
    
    @Override
    public void notify(Move move) {
        if (move.isDoMove()) {
            if (move.getPlayer() == Game.COMPUTER) {
                ++computer;
            }
            else if (move.getPlayer() == Game.HUMAN) {
                ++human;
            }
        } else if (move.getPrevPlayer() == Game.HUMAN) {
            --human;
        } else if (move.getPrevPlayer() == Game.COMPUTER) {
            --computer;
        }
    }

    @Override
    public String toString() {
        String str = "{ ";
        for (Space space : spaceList) {
            str += "(" + space.getRow() + ", " + space.getCol() + ") ";
        }
        str += ": " + score() + "[" + human + ":" + computer + "] }";
        return str;
    }
    
    

}
