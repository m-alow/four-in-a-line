/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fourinline;

import java.util.ArrayList;

/**
 *
 * @author Alow, M
 */
public class Space {
    private int row = 0;
    private int col = 0;
    private final ArrayList<SpceObserver> observerList = new ArrayList<>();

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public void setCol(int col) {
        this.col = col;
    }
    
    public void registerObserver(SpceObserver observer) {
        observerList.add(observer);
    }
    
    public void unregisterObserver(SpceObserver observer) {
        observerList.remove(observer);
    }
    
    public void notifyObservers(Move move) {
        for (SpceObserver observer : observerList) {
            observer.notify(move);
        }
    }
    
    public int score() {
        int res = 0;
        for (SpceObserver observer : observerList) {
            res += observer.score();
        }
        return res;
    }
}
