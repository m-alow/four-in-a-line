package fourinline;

/**
 *
 * @author Alow, M
 */
public class Board implements Cloneable {
    public static final int FREE = 0;
    public static final int PLAYER1 = 1;
    public static final int PLAYER2 = 2;
    private final int rowNo;
    private final int colNo;
    
    private int[][] board;
    private int[] topFreeSpace;

    private int playedMovesNo = 0;
    
    public Board() {
        this(6, 7); 
    }

    public Board(int rowNo, int colNo) {
        this.rowNo = rowNo;
        this.colNo = colNo;
        board = new int[rowNo][colNo];
        topFreeSpace = new int[colNo];
    }

    public int getRowNo() {
        return rowNo;
    }

    public int getColNo() {
        return colNo;
    }
    
    public Move availableMove(int col, int player) {
        if (isFilledColumn(col)) return null;
        int row = topFreeSpace[col];
        return new Move(row, col, player, Board.FREE);
    }
    
    public void play(Move move) {
        int moveCol = move.getCol();
        int moveRow = move.getRow();
        if (move.isDoMove()) {
            ++topFreeSpace[moveCol];
            ++playedMovesNo;
        } else {
            --topFreeSpace[moveCol];
            --playedMovesNo;
        }
        board[moveRow][moveCol] = move.getPlayer();
    }
    
    public boolean isFilled() {
        return playedMovesNo == rowNo * colNo;
    }
    
    private boolean isFilledColumn(int col) {
        return topFreeSpace[col] == rowNo;
    }

    public int[][] getBoard() {
        int[][] res = board.clone();
        for (int i = 0; i < board.length; i++) {
            res[i] = board[i].clone();
        }
        return res;
    }
    
    
}


