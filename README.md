# Four in a Line
This is a project for an undergraduate course in *Intelligent search algorithms*, Fall 2015 at *Damascus university*.
**Four in a line** (also known as **Connect Four**) is a two-players strategy board game. Find more at [wikipedia](https://en.wikipedia.org/wiki/Connect_Four).

This program uses [*Minimax*](https://en.wikipedia.org/wiki/Minimax) sesarch algorithm with [*Alpha-beta pruning*](https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning). It also uses an **incremental** evaluation function. It's implemented using *java*.
## Search algorithm

## Evaluation function

## Results